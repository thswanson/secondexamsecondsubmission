package alphacivsrc.domain;

public interface UnitActions {
	public String getActionName();
	public void unitPreformsAction(UnitContext unitContext);
}
