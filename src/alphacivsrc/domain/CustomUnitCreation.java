package alphacivsrc.domain;

public class CustomUnitCreation implements UnitCreationFactory {

	UnitImpl settler;
	UnitImpl legion;
	UnitImpl archer;
	
	UnitActions ArcherAction;
	UnitActions SettlerAction;
	UnitActions LegionAction;
	
	public CustomUnitCreation () {
		ArcherAction  = new ArcherActionImpl() ;
		SettlerAction = new SettlerAction();
		LegionAction = new NoUnitActionImpl();
	}

	@Override
	public UnitImpl createUnit(Player owner, String unitType) {
		if(unitType.equals(GameConstants.ARCHER)) {
			UnitImpl unit = new UnitImpl(owner, unitType, ArcherAction);
			return unit;
		}else if(unitType.equals(GameConstants.SETTLER)) {
			UnitImpl unit = new UnitImpl(owner, unitType,SettlerAction);
			return unit;
		}
		UnitImpl unit = new UnitImpl(owner,unitType, LegionAction);
		return unit;
	}

	@Override
	public void setUnitAction(String unitType, UnitActions unitAction) {
		switch(unitType) {
			case GameConstants.ARCHER: 
				ArcherAction = unitAction;
				break;
			case GameConstants.SETTLER: 
				SettlerAction = unitAction;
				break;
			case GameConstants.LEGION: 
				LegionAction = unitAction;
				break;
		}
		System.out.println(unitAction.getActionName());
	}
	
	 
	public UnitActions getArcherAction() {
		return ArcherAction;
	}
	public UnitActions getSettlerAction() {
		return SettlerAction;
	}
	public UnitActions getLegionAction() {
		return LegionAction;
	}

}
