package alphacivsrc.domain;

public class SettlerAction implements UnitActions {

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		TileImpl tile = unitContext.getTile();
		UnitImpl unit = tile.getUnit();
		tile.setCity(new CityImpl(unit.getOwner(), tile.getPosition()));
	}

	@Override
	public String getActionName() {
		return "settled";
	}

}
