package alphacivsrc.domain;

public class NoUnitActionImpl implements UnitActions {

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		// do nothing

	}

	@Override
	public String getActionName() {
		return "nothing";
	}

}
