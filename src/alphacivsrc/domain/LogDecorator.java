package alphacivsrc.domain;

public class LogDecorator implements Game {
	
	
	private Game game;
	private boolean transcriptOn;
	CivTranscriptBuilder transcript;
	
	public LogDecorator (GameImpl game, boolean transcriptOn) {
		this.game = game;
		this.transcriptOn = transcriptOn;
		transcript = new CivTranscriptBuilder();
	}

	@Override
	public Tile getTileAt(Position p) {
		return game.getTileAt(p);
	}

	@Override
	public Unit getUnitAt(Position p) {
		// TODO Auto-generated method stub
		return game.getUnitAt(p);
	}

	@Override
	public City getCityAt(Position p) {
		// TODO Auto-generated method stub
		return game.getCityAt(p);
	}

	@Override
	public Player getPlayerInTurn() {
		// TODO Auto-generated method stub
		return game.getPlayerInTurn();
	}

	@Override
	public Player getWinner() {
		// TODO Auto-generated method stub
		return game.getWinner();
	}

	@Override
	public int getAge() {
		// TODO Auto-generated method stub
		return game.getAge();
	}

	@Override
	public boolean moveUnit(Position from, Position to) {
		boolean validMove = game.moveUnit(from, to);
		if (validMove && transcriptOn) {
			transcript.buildMovementTranscript(from, to, (UnitImpl) getUnitAt(to), getUnitAt(to).getOwner());
		}
		return validMove;
	}

	@Override
	public void endOfTurn() {
		if (transcriptOn) {
			transcript.buildEndOFTurn(getPlayerInTurn());
		}
		game.endOfTurn();
	}

	@Override
	public void changeWorkForceFocusInCityAt(Position p, String balance) {
		if(transcriptOn) {
			transcript.buildWorkFocusTranscript((CityImpl) getCityAt(p), balance, getCityAt(p).getOwner());
		}
		game.changeWorkForceFocusInCityAt(p, balance);
	}

	@Override
	public void changeProductionInCityAt(Position p, String unitType) {
		game.changeProductionInCityAt(p, unitType);
		if(transcriptOn) {
			transcript.buildChangeTranscript((CityImpl) getCityAt(p), getCityAt(p).getProduction(), getCityAt(p).getOwner());
		}
	}

	@Override
	public void performUnitActionAt(Position p) {
		if(transcriptOn) {
			transcript.buildActionTranscript((UnitImpl) getUnitAt(p), getUnitAt(p).getOwner(), 
					((UnitImpl)getUnitAt(p)).getUnitAction(), p);
		}
		game.performUnitActionAt(p);
	}
	
	public Transcriber getTranscript() {
		return transcript;
	}
	
	public void toggleTranscript() {
		if(transcriptOn) transcriptOn = false;
		else transcriptOn = true; 
	}

}
