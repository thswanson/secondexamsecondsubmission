package alphacivsrc.domain;

public interface Transcriber {
	
	public String transcriptOut();
	public void buildMovementTranscript(Position to, Position from, UnitImpl unit, Player player);
	public void buildActionTranscript(UnitImpl unit, Player player, UnitActions unitAction, Position pos);
	public void buildBuildTranscript(CityImpl city,  Position placed, String unit, Player player);
	public void buildChangeTranscript(CityImpl city,  String unit, Player player);
	void buildWorkFocusTranscript(CityImpl city, String focus, Player player);
	

}
