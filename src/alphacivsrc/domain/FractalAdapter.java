package alphacivsrc.domain;

import java.util.ArrayList;

import thirdparty.*;
//import  thirdparty.ThirdPartyFractalGenerator;
//import thirdparty.ThirdPartyFractalGenerator;

public class FractalAdapter implements BoardSetUp {
	thirdparty.ThirdPartyFractalGenerator fractalGen = new thirdparty.ThirdPartyFractalGenerator();
	//thirdparty = new thirdparty.ThirdPartyFractalGenerator()
	
	TileImpl[][] boardArray;
	
	public FractalAdapter() {
		//fractalGen = new thirdparty.ThirdPartyFractalGenerator;
		boardArray = new TileImpl[16][16];
	}
	
	public TileImpl terrainToPlace(Position pos) {
		char toMake =  fractalGen.getLandscapeAt(pos.getRow(), pos.getColumn());
		String terrain;
		//System.out.println(toMake);
		switch(toMake) {
			case '.': 
				terrain = GameConstants.OCEANS;
				break;
			case 'o':
				terrain = GameConstants.PLAINS;
				break;
			case 'f':
				terrain = GameConstants.FOREST;
				break;
			case 'h':
				terrain = GameConstants.HILLS;
				break;
			case 'M':
				terrain = GameConstants.MOUNTAINS;
				break;
			default:
				terrain = "Invalid";
				break;
		}
		//System.out.println(terrain);
		TileImpl tile; 
		if (!terrain.equals("invalid")) tile = new TileImpl(pos, terrain); 
		else tile = new TileImpl(pos, GameConstants.PLAINS); 
		
		
		return tile;
	}
	
	@Override
	public TileImpl[][] setUpBoard(UnitCreationFactory unitFactory) {
		for (int i =0; i < 16; i++) {
			for (int j = 0; j<16;j++) {
				boardArray[i][j] = terrainToPlace(new Position(i,j));
			}
		}
		return boardArray;
	}
	public thirdparty.ThirdPartyFractalGenerator getFractalThird() {
		return fractalGen;
	}

	@Override
	public ArrayList<CityImpl> getCities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<UnitImpl> getUnits() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
