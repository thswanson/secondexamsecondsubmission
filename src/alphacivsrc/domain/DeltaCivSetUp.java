package alphacivsrc.domain;

import java.util.ArrayList;

public class DeltaCivSetUp implements BoardSetUp {
	private TileImpl[][] boardArray;
	ArrayList<CityImpl> cities;
	ArrayList<UnitImpl> units;

	public DeltaCivSetUp(TileImpl[][] boardArray) {
		this.boardArray = boardArray;
		cities = new ArrayList<CityImpl>();
		units = new ArrayList<UnitImpl>();
	}

	public DeltaCivSetUp() {
		boardArray = new TileImpl[0][0];
		cities = new ArrayList<CityImpl>();
		units = new ArrayList<UnitImpl>();
	}

	@Override
	public TileImpl[][] setUpBoard(UnitCreationFactory unitFactory) {
		TileImpl[][] boardArray = new TileImpl[GameConstants.WORLDSIZE][GameConstants.WORLDSIZE];
		placeOceans(boardArray);
		placeMountains(boardArray);
		placeHills(boardArray);
		placeForests(boardArray);
		for (int row = 0; row < GameConstants.WORLDSIZE; row++) {
			for (int column = 0; column < GameConstants.WORLDSIZE; column++) {
				if (boardArray[row][column] == null) {
					boardArray[row][column] = new TileImpl(new Position(row, column), GameConstants.PLAINS);
				}
			}
		}
		setCities(boardArray);
		setUnit(boardArray);
		return boardArray;
	}

	private void setUnit(TileImpl[][] boardArray2) {
		UnitImpl archer = new UnitImpl(Player.RED, GameConstants.ARCHER, new NoUnitActionImpl());
		units.add(archer);
		boardArray2[3][8].setUnit(archer);
		UnitImpl legion = new UnitImpl(Player.RED, GameConstants.LEGION, new NoUnitActionImpl());
		units.add(legion);
		boardArray2[4][4].setUnit(legion);
		UnitImpl settler = new UnitImpl(Player.RED, GameConstants.SETTLER, new NoUnitActionImpl());
		units.add(settler);
		boardArray2[5][5].setUnit(settler);
	}

	private void setCities(TileImpl[][] boardArray2) {
		CityImpl redCity = new CityImpl(Player.RED, new Position(4,5));
		cities.add(redCity);
		boardArray2[4][5].setCity(redCity);
		CityImpl blueCity = new CityImpl(Player.BLUE, new Position(8, 12));
		cities.add(blueCity);
		boardArray2[8][12].setCity(blueCity);
	}

	private void placeOceans(TileImpl[][] boardArray) {
		// Row 0
		boardArray[0][0] = new TileImpl(new Position(0, 0), GameConstants.OCEANS);
		boardArray[0][1] = new TileImpl(new Position(0, 1), GameConstants.OCEANS);
		boardArray[0][2] = new TileImpl(new Position(0, 2), GameConstants.OCEANS);
		boardArray[0][11] = new TileImpl(new Position(0, 11), GameConstants.OCEANS);
		boardArray[0][12] = new TileImpl(new Position(0, 12), GameConstants.OCEANS);
		boardArray[0][13] = new TileImpl(new Position(0, 13), GameConstants.OCEANS);
		boardArray[0][14] = new TileImpl(new Position(0, 14), GameConstants.OCEANS);
		boardArray[0][15] = new TileImpl(new Position(0, 15), GameConstants.OCEANS);
		// Row 1
		boardArray[1][0] = new TileImpl(new Position(1, 0), GameConstants.OCEANS);
		boardArray[1][1] = new TileImpl(new Position(1, 1), GameConstants.OCEANS);
		boardArray[1][14] = new TileImpl(new Position(1, 14), GameConstants.OCEANS);
		boardArray[1][15] = new TileImpl(new Position(1, 15), GameConstants.OCEANS);
		// Row 2
		boardArray[2][0] = new TileImpl(new Position(2, 0), GameConstants.OCEANS);
		boardArray[2][10] = new TileImpl(new Position(2, 10), GameConstants.OCEANS);
		boardArray[2][11] = new TileImpl(new Position(2, 11), GameConstants.OCEANS);
		boardArray[2][12] = new TileImpl(new Position(2, 12), GameConstants.OCEANS);
		boardArray[2][15] = new TileImpl(new Position(2, 15), GameConstants.OCEANS);
		// Row 3
		boardArray[3][0] = new TileImpl(new Position(3, 0), GameConstants.OCEANS);
		boardArray[3][10] = new TileImpl(new Position(3, 10), GameConstants.OCEANS);
		boardArray[3][11] = new TileImpl(new Position(3, 11), GameConstants.OCEANS);
		// Row 4
		boardArray[4][0] = new TileImpl(new Position(4, 0), GameConstants.OCEANS);
		boardArray[4][1] = new TileImpl(new Position(4, 1), GameConstants.OCEANS);
		boardArray[4][2] = new TileImpl(new Position(4, 2), GameConstants.OCEANS);
		boardArray[4][14] = new TileImpl(new Position(4, 14), GameConstants.OCEANS);
		boardArray[4][15] = new TileImpl(new Position(4, 15), GameConstants.OCEANS);
		// Row 5
		boardArray[5][0] = new TileImpl(new Position(5, 15), GameConstants.OCEANS);
		boardArray[5][15] = new TileImpl(new Position(5, 15), GameConstants.OCEANS);
		// Row 6
		boardArray[6][0] = new TileImpl(new Position(6, 0), GameConstants.OCEANS);
		boardArray[6][1] = new TileImpl(new Position(6, 1), GameConstants.OCEANS);
		boardArray[6][2] = new TileImpl(new Position(6, 2), GameConstants.OCEANS);
		boardArray[6][6] = new TileImpl(new Position(6, 6), GameConstants.OCEANS);
		boardArray[6][7] = new TileImpl(new Position(6, 7), GameConstants.OCEANS);
		boardArray[6][8] = new TileImpl(new Position(6, 8), GameConstants.OCEANS);
		boardArray[6][9] = new TileImpl(new Position(6, 9), GameConstants.OCEANS);
		boardArray[6][10] = new TileImpl(new Position(6, 10), GameConstants.OCEANS);
		boardArray[6][11] = new TileImpl(new Position(6, 11), GameConstants.OCEANS);
		boardArray[6][12] = new TileImpl(new Position(6, 12), GameConstants.OCEANS);
		boardArray[6][13] = new TileImpl(new Position(6, 13), GameConstants.OCEANS);
		boardArray[6][14] = new TileImpl(new Position(6, 14), GameConstants.OCEANS);
		boardArray[6][15] = new TileImpl(new Position(6, 15), GameConstants.OCEANS);
		// Row 7
		boardArray[7][0] = new TileImpl(new Position(7, 0), GameConstants.OCEANS);
		boardArray[7][6] = new TileImpl(new Position(7, 6), GameConstants.OCEANS);
		boardArray[7][14] = new TileImpl(new Position(7, 14), GameConstants.OCEANS);
		boardArray[7][15] = new TileImpl(new Position(7, 15), GameConstants.OCEANS);
		// Row 8
		boardArray[8][0] = new TileImpl(new Position(8, 0), GameConstants.OCEANS);
		boardArray[8][6] = new TileImpl(new Position(8, 6), GameConstants.OCEANS);
		boardArray[8][14] = new TileImpl(new Position(8, 14), GameConstants.OCEANS);
		boardArray[8][15] = new TileImpl(new Position(8, 15), GameConstants.OCEANS);
		// Row 9
		boardArray[9][8] = new TileImpl(new Position(9, 8), GameConstants.OCEANS);
		// Row 10
		boardArray[10][8] = new TileImpl(new Position(10, 8), GameConstants.OCEANS);
		boardArray[10][9] = new TileImpl(new Position(10, 9), GameConstants.OCEANS);
		boardArray[10][10] = new TileImpl(new Position(10, 10), GameConstants.OCEANS);
		// Row 11
		boardArray[11][10] = new TileImpl(new Position(11, 10), GameConstants.OCEANS);
		boardArray[11][11] = new TileImpl(new Position(11, 11), GameConstants.OCEANS);
		boardArray[11][12] = new TileImpl(new Position(11, 12), GameConstants.OCEANS);
		boardArray[11][13] = new TileImpl(new Position(11, 13), GameConstants.OCEANS);
		boardArray[11][14] = new TileImpl(new Position(11, 14), GameConstants.OCEANS);
		boardArray[11][15] = new TileImpl(new Position(11, 15), GameConstants.OCEANS);
		// Row 12
		boardArray[12][0] = new TileImpl(new Position(12, 0), GameConstants.OCEANS);
		boardArray[12][1] = new TileImpl(new Position(12, 1), GameConstants.OCEANS);
		boardArray[12][14] = new TileImpl(new Position(12, 14), GameConstants.OCEANS);
		boardArray[12][15] = new TileImpl(new Position(12, 15), GameConstants.OCEANS);
		// Row 13
		boardArray[13][0] = new TileImpl(new Position(13, 0), GameConstants.OCEANS);
		boardArray[13][1] = new TileImpl(new Position(13, 1), GameConstants.OCEANS);
		boardArray[13][2] = new TileImpl(new Position(13, 2), GameConstants.OCEANS);
		boardArray[13][3] = new TileImpl(new Position(13, 3), GameConstants.OCEANS);
		boardArray[13][13] = new TileImpl(new Position(13, 13), GameConstants.OCEANS);
		boardArray[13][14] = new TileImpl(new Position(13, 14), GameConstants.OCEANS);
		boardArray[13][15] = new TileImpl(new Position(13, 15), GameConstants.OCEANS);
		// Row 14
		boardArray[14][0] = new TileImpl(new Position(14, 0), GameConstants.OCEANS);
		boardArray[14][1] = new TileImpl(new Position(14, 1), GameConstants.OCEANS);
		boardArray[14][9] = new TileImpl(new Position(14, 9), GameConstants.OCEANS);
		boardArray[14][10] = new TileImpl(new Position(14, 10), GameConstants.OCEANS);
		boardArray[14][11] = new TileImpl(new Position(14, 11), GameConstants.OCEANS);
		boardArray[14][12] = new TileImpl(new Position(14, 12), GameConstants.OCEANS);
		boardArray[14][13] = new TileImpl(new Position(14, 13), GameConstants.OCEANS);
		boardArray[14][14] = new TileImpl(new Position(14, 14), GameConstants.OCEANS);
		boardArray[14][15] = new TileImpl(new Position(14, 15), GameConstants.OCEANS);
		// Row 15
		boardArray[15][0] = new TileImpl(new Position(15, 0), GameConstants.OCEANS);
		boardArray[15][1] = new TileImpl(new Position(15, 1), GameConstants.OCEANS);
		boardArray[15][2] = new TileImpl(new Position(15, 2), GameConstants.OCEANS);
		boardArray[15][3] = new TileImpl(new Position(15, 3), GameConstants.OCEANS);
		boardArray[15][4] = new TileImpl(new Position(15, 4), GameConstants.OCEANS);
		boardArray[15][14] = new TileImpl(new Position(15, 14), GameConstants.OCEANS);
		boardArray[15][15] = new TileImpl(new Position(15, 15), GameConstants.OCEANS);
	}

	private void placeMountains(TileImpl[][] boardArray) {
		//Row0
		boardArray[0][5] = new TileImpl(new Position(0, 5), GameConstants.MOUNTAINS);
		//Row2
		boardArray[2][6] = new TileImpl(new Position(2,6), GameConstants.MOUNTAINS);
		//Row3
		boardArray[3][3] = new TileImpl(new Position(3,3), GameConstants.MOUNTAINS);
		boardArray[3][4] = new TileImpl(new Position(3,4), GameConstants.MOUNTAINS);
		boardArray[3][5] = new TileImpl(new Position(3,5), GameConstants.MOUNTAINS);
		//Row7
		boardArray[7][13] = new TileImpl(new Position(7,13), GameConstants.MOUNTAINS);
		//Row11
		boardArray[11][3] = new TileImpl(new Position(11,3), GameConstants.MOUNTAINS);
		boardArray[11][4] = new TileImpl(new Position(11,4), GameConstants.MOUNTAINS);
		boardArray[11][5] = new TileImpl(new Position(11,5), GameConstants.MOUNTAINS);
	}

	private void placeHills(TileImpl[][] boardArray) {
		//Row 1
		boardArray[1][3] = new TileImpl(new Position(1,3), GameConstants.HILLS);
		boardArray[1][4] = new TileImpl(new Position(1,4), GameConstants.HILLS);
		//Row 4
		boardArray[4][8] = new TileImpl(new Position(4,8), GameConstants.HILLS);
		boardArray[4][9] = new TileImpl(new Position(4,9), GameConstants.HILLS);
		//Row 5
		boardArray[5][11] = new TileImpl(new Position(5,11), GameConstants.HILLS);
		boardArray[5][12] = new TileImpl(new Position(5,12), GameConstants.HILLS);
		//Row 7
		boardArray[7][10] = new TileImpl(new Position(7,10), GameConstants.HILLS);
		//Row 8
		boardArray[8][9] = new TileImpl(new Position(8,9), GameConstants.HILLS);
		//Row 14
		boardArray[14][5] = new TileImpl(new Position(14,5), GameConstants.HILLS);
		boardArray[14][6] = new TileImpl(new Position(14,6), GameConstants.HILLS);
	}

	private void placeForests(TileImpl[][] boardArray) {
		//Row 1
		boardArray[1][9] = new TileImpl(new Position(1,9), GameConstants.FOREST);
		boardArray[1][10] = new TileImpl(new Position(1,10), GameConstants.FOREST);
		boardArray[1][11] = new TileImpl(new Position(1,11), GameConstants.FOREST);
		//Row 5
		boardArray[5][2] = new TileImpl(new Position(5,2), GameConstants.FOREST);
		//Row 8
		boardArray[8][13] = new TileImpl(new Position(8,13), GameConstants.FOREST);
		//Row 9
		boardArray[9][1] = new TileImpl(new Position(9,1), GameConstants.FOREST);
		boardArray[9][2] = new TileImpl(new Position(9,2), GameConstants.FOREST);
		boardArray[9][3] = new TileImpl(new Position(9,3), GameConstants.FOREST);
		boardArray[9][10] = new TileImpl(new Position(9,10), GameConstants.FOREST);
		boardArray[9][11] = new TileImpl(new Position(9,11), GameConstants.FOREST);
		//Row 12
		boardArray[12][8] = new TileImpl(new Position(12,8), GameConstants.FOREST);
		boardArray[12][9] = new TileImpl(new Position(12,9), GameConstants.FOREST);
	}


	@Override
	public ArrayList<CityImpl> getCities() {
		return cities;
	}

	@Override
	public ArrayList<UnitImpl> getUnits() {
		return units;
	}

}
