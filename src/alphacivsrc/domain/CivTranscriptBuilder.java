package alphacivsrc.domain;

public class CivTranscriptBuilder implements Transcriber {
	private String transcript = "";
	
	@Override
	public String transcriptOut() {
		return transcript;
	}

	@Override
	public void buildMovementTranscript(Position from,Position to, UnitImpl unit, Player player) {
		transcript += player + " moved unit " + unit.getTypeString() +
				" from " + from.getRow() + ","+ from.getColumn() + " to " + to.getRow() + ","+to.getColumn()+"\n";
		System.out.println(transcript);
		
	}

	@Override
	public void buildActionTranscript(UnitImpl unit, Player player, UnitActions unitAction, Position pos) {
		transcript += player + " " + unit.getTypeString() + " " +  unitAction.getActionName()+" at "+pos.getRow()+","+pos.getColumn()+"\n";
		System.out.println(transcript);
	}

	@Override
	public void buildBuildTranscript(CityImpl city, Position placed, String unit, Player player) {
		//transcript += player + " city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " produced " + unit+"\n";
		System.out.println(transcript);
	}

	@Override
	public void buildChangeTranscript(CityImpl city, String unit, Player player) {
		// TODO Auto-generated method stub
		transcript += player + " changes production focus in city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " to " + city.getProduction()+"\n";
		System.out.println(transcript);
	}
	@Override
	public void buildWorkFocusTranscript(CityImpl city, String focus, Player player) {
		// TODO Auto-generated method stub
		transcript += player + " changes work focus in city at "+city.getPosition().getRow()+","+city.getPosition().getColumn() + " to " + focus +"\n";
		System.out.println(transcript);
	}

	public void buildEndOFTurn(Player playerInTurn) {
		transcript += playerInTurn + " ended the turn\n";
		System.out.println(transcript);
		
	}
	
	

}
