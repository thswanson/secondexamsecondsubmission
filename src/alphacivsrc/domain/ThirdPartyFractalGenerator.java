package alphacivsrc.domain;
 

public interface ThirdPartyFractalGenerator {
	/**
	 * get a character that indicates the type of terrain
	 * at the given position(row, column).
	 * @param row the row of the position of interest
	 * @param col the column of the position of interest
	 * @return a character defining the type of terrain
	 *         at the specified position. Five types of
	 *         of terrain are possible: ‘.’ is ocean,
	 *         ‘o’ is plain, ‘f’ is forest, ‘h’ is hill,
	 *         ‘M’ is mountain.
	 */
	public char getTerrainAt( int row, int col );
}
