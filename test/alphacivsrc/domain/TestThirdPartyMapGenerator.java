package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import thirdparty.ThirdPartyFractalGenerator;

public class TestThirdPartyMapGenerator {

	private GameImpl game;
	private TileImpl[][] boardArray;
	private FractalAdapter fractalAdapter;
	
	@Before
	public void setUp() throws Exception {
		//testTerrain = new ThirdPartyFractalGenerator();
		fractalAdapter = new FractalAdapter();
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), fractalAdapter, new UnitCreationAlphaCiv());
		boardArray = ((GameImpl) game).getBoardArray();
	}
	
	@Test
	public void testBoardSetup() {
		for (int i = 0; i < 16 ; i++) {
			for (int j = 0; j < 16; j++) {
				String terrain;
				switch(fractalAdapter.getFractalThird().getLandscapeAt(i, j)) {
				case '.': 
					terrain = GameConstants.OCEANS;
					break;
				case 'o':
					terrain = GameConstants.PLAINS;
					break;
				case 'f':
					terrain = GameConstants.FOREST;
					break;
				case 'h':
					terrain = GameConstants.HILLS;
					break;
				case 'M':
					terrain = GameConstants.MOUNTAINS;
					break;
				default:
					terrain = "Invalid";
					break;
				}
				//System.out.println(terrain);
				//dSystem.out.println(boardArray[i][j].getTypeString());
				assertTrue(boardArray[i][j].getTypeString().equals(terrain));
			}
		}
	}
	
	

}
