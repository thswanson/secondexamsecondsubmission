package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TestCustomUnitActions {
	
	@Test
	public void testChangeUnitAction() {
		CustomUnitCreation CustomUnitCreation = new CustomUnitCreation();
		UnitImpl unit = CustomUnitCreation.createUnit(Player.RED, GameConstants.ARCHER);
		assertTrue(unit.getUnitAction().equals(CustomUnitCreation.getArcherAction()));
		CustomUnitCreation.setUnitAction(GameConstants.ARCHER, new SettlerAction());
		//System.out.print(CustomUnitCreation.getArcherAction().getActionName());
		UnitImpl unit2 = CustomUnitCreation.createUnit(Player.RED, GameConstants.ARCHER);
		assertTrue(unit2.getUnitAction().getActionName().equals(CustomUnitCreation.getSettlerAction().getActionName()));
	}
	
	@Test
	public void customUnitAction() {
		CustomUnitCreation CustomUnitCreation = new CustomUnitCreation();
		UnitImpl unit = CustomUnitCreation.createUnit(Player.RED, GameConstants.ARCHER);
		assertTrue(unit.getUnitAction().equals(CustomUnitCreation.getArcherAction()));
		CustomUnitCreation.setUnitAction(GameConstants.ARCHER, new CustomUnitAction());
		//System.out.print(CustomUnitCreation.getArcherAction().getActionName());
		UnitImpl unit2 = CustomUnitCreation.createUnit(Player.RED, GameConstants.ARCHER);
		assertTrue(unit2.getUnitAction().getActionName().equals(new CustomUnitAction().getActionName()));
		
	}
	
	@Test
	public void customUnitActionWithoutFactory() {
		UnitImpl unit = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		CustomUnitAction cua = new CustomUnitAction();
		unit.setUnitAction(cua);
		assertEquals(unit.getUnitAction(), cua);
	}
	
	

}
class CustomUnitAction implements UnitActions {

	@Override
	public String getActionName() {
		// TODO Auto-generated method stub
		return "brandNewClass";
	}

	@Override
	public void unitPreformsAction(UnitContext unitContext) {
		UnitImpl unit = unitContext.getTile().getUnit();
		unit.setDefensiveStrength(10000);
		
	}
	
}