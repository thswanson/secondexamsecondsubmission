package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestDeltaCiv {
	private GameImpl game;
	private TileImpl[][] boardArray;
	
	@Before
	public void setUp() throws Exception {
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new DeltaCivSetUp(), new UnitCreationAlphaCiv());
		boardArray = ((GameImpl) game).getBoardArray();
	}

	@Test
	public void testForOceans() {
		assertEquals(GameConstants.OCEANS, boardArray[0][0].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[0][14].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[0][15].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[1][1].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[2][10].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[3][11].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[4][14].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[5][15].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[6][2].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[6][12].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[7][6].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[8][14].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[12][14].getTypeString());
		
		assertEquals(GameConstants.OCEANS, boardArray[15][0].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[15][4].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[15][14].getTypeString());
		assertEquals(GameConstants.OCEANS, boardArray[15][15].getTypeString());
	}
	
	@Test
	public void testForUnit() {
		assertEquals(GameConstants.ARCHER, boardArray[3][8].getUnit().getTypeString());
		
		assertEquals(GameConstants.LEGION, boardArray[4][4].getUnit().getTypeString());
		
		assertEquals(GameConstants.SETTLER, boardArray[5][5].getUnit().getTypeString());
	}
	
	@Test
	public void testForCity() {
		assertNotNull(boardArray[4][5].getCity());
		assertNotNull(boardArray[8][12]);
	}

	@Test
	public void testForMountains() {
		assertEquals(GameConstants.MOUNTAINS, boardArray[0][5].getTypeString());
		assertEquals(GameConstants.MOUNTAINS, boardArray[3][4].getTypeString());
		assertEquals(GameConstants.MOUNTAINS, boardArray[11][5].getTypeString());
	}
	
	@Test
	public void testForHills() {
		assertEquals(GameConstants.HILLS, boardArray[1][4].getTypeString());
		assertEquals(GameConstants.HILLS, boardArray[5][11].getTypeString());
		assertEquals(GameConstants.HILLS, boardArray[7][10].getTypeString());
		assertEquals(GameConstants.HILLS, boardArray[14][5].getTypeString());
	}
	
	@Test
	public void testForForrest() {
		assertEquals(GameConstants.FOREST, boardArray[1][9].getTypeString());
		assertEquals(GameConstants.FOREST, boardArray[1][11].getTypeString());
		assertEquals(GameConstants.FOREST, boardArray[5][2].getTypeString());
		assertEquals(GameConstants.FOREST, boardArray[9][2].getTypeString());
		assertEquals(GameConstants.FOREST, boardArray[9][10].getTypeString());		assertEquals(GameConstants.FOREST, boardArray[1][9].getTypeString());
		assertEquals(GameConstants.FOREST, boardArray[12][9].getTypeString());
	}
}
