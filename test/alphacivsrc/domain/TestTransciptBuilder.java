package alphacivsrc.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestTransciptBuilder {
	@Test
	public void testMovementTranscript() {
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		UnitImpl arch = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		transcript.buildMovementTranscript(new Position(1,0), new Position(1,1), arch , arch.getOwner());
		String testEquals = transcript.transcriptOut();
		assertTrue(testEquals.equals("RED moved unit archer from 1,0 to 1,1\n"));
	}
	
	@Test
	public void testActionTranscript() {
		
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		UnitImpl arch = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		transcript.buildActionTranscript(arch, arch.getOwner(), arch.getUnitAction(), new Position(1,0));
		String testEquals= transcript.transcriptOut();
		assertTrue(testEquals.equals("RED archer fortified at 1,0\n"));

	}
	
	@Test
	public void testBuildChangeTranscript() {
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		CityImpl city = new CityImpl(Player.RED, new Position(1,0));
		city.setProductionType(GameConstants.ARCHER);
		city.setProductionType(GameConstants.LEGION);
		transcript.buildChangeTranscript(city, city.getProduction(), city.getOwner());
		String testEquals= transcript.transcriptOut();
		assertTrue(testEquals.equals("RED changes production focus in city at 1,0 to legion\n"));
	}
	
	@Test
	public void testBuildFocusTranscript() {
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		CityImpl city = new CityImpl(Player.RED, new Position(1,0));
		transcript.buildWorkFocusTranscript(city, GameConstants.foodFocus, city.getOwner());
		String testEquals= transcript.transcriptOut();
		assertTrue(testEquals.equals("RED changes work focus in city at 1,0 to apple\n"));
	}
	
	@Test
	public void testMultipleActions() {
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		CityImpl city = new CityImpl(Player.RED, new Position(1,0));
		city.setProductionType(GameConstants.ARCHER);
		city.setProductionType(GameConstants.LEGION);
		transcript.buildChangeTranscript(city, city.getProduction(), city.getOwner());
		String testEquals= transcript.transcriptOut();
		assertTrue(testEquals.equals("RED changes production focus in city at 1,0 to legion\n"));
		
		CityImpl city1 = new CityImpl(Player.BLUE, new Position(2,0));
		city.setProductionType(GameConstants.LEGION);
		city.setProductionType(GameConstants.ARCHER);
		transcript.buildChangeTranscript(city1, city1.getProduction(), city1.getOwner());
		String testEquals1 = transcript.transcriptOut();
		assertTrue(testEquals1.equals("RED changes production focus in city at 1,0 to legion\n"
				+ "BLUE changes production focus in city at 2,0 to archer\n"));
		transcript.buildWorkFocusTranscript(city, GameConstants.foodFocus, city.getOwner());
		String testEquals2= transcript.transcriptOut();
		assertTrue(testEquals2.equals("RED changes production focus in city at 1,0 to legion\n" + 
					"BLUE changes production focus in city at 2,0 to archer\n"
						+ "RED changes work focus in city at 1,0 to apple\n"));
		transcript.buildWorkFocusTranscript(city1, GameConstants.foodFocus, city1.getOwner());
		String testEquals3= transcript.transcriptOut();
		assertTrue(testEquals3.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+ "BLUE changes work focus in city at 2,0 to apple\n"));
		
		UnitImpl arch = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		transcript.buildActionTranscript(arch, arch.getOwner(), arch.getUnitAction(), new Position(1,0));
		String testEquals4= transcript.transcriptOut();
		assertTrue(testEquals4.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+ "BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"));
		UnitImpl settler = new UnitImpl(Player.BLUE, GameConstants.SETTLER, new SettlerAction());
		transcript.buildActionTranscript(settler, settler.getOwner(), settler.getUnitAction(), new Position(12,0));
		String testEquals5 = transcript.transcriptOut();
		assertTrue(testEquals5.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+ "BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"+
				"BLUE settler settled at 12,0\n"));
		city.setProductionType(GameConstants.ARCHER);
		transcript.buildBuildTranscript(city, city.getPosition(), city.getProduction(), city.getOwner());
		String testEquals6= transcript.transcriptOut();
		assertTrue(testEquals6.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+
				"BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"+
				"BLUE settler settled at 12,0\n" 
			));
		city1.setProductionType(GameConstants.SETTLER);
		transcript.buildBuildTranscript(city1, city1.getPosition(), city1.getProduction(), city1.getOwner());
		String testEquals7= transcript.transcriptOut();
		assertTrue(testEquals7.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+
				"BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"+
				"BLUE settler settled at 12,0\n"));
		UnitImpl arch1 = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		transcript.buildMovementTranscript(new Position(1,0), new Position(1,1), arch1 , arch1.getOwner());
		String testEquals8 = transcript.transcriptOut();
		assertTrue(testEquals8.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+
				"BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"+
				"BLUE settler settled at 12,0\n" +
				 
				"RED moved unit archer from 1,0 to 1,1\n"));
		UnitImpl arch2 = new UnitImpl(Player.BLUE, GameConstants.ARCHER, new ArcherActionImpl());
		transcript.buildMovementTranscript(new Position(1,0), new Position(1,1), arch2 , arch2.getOwner());
		String testEquals9 = transcript.transcriptOut();
		assertTrue(testEquals9.equals("RED changes production focus in city at 1,0 to legion\n" + 
				"BLUE changes production focus in city at 2,0 to archer\n"
				+ "RED changes work focus in city at 1,0 to apple\n"+
				"BLUE changes work focus in city at 2,0 to apple\n"+
				"RED archer fortified at 1,0\n"+
				"BLUE settler settled at 12,0\n" +
				 
				"RED moved unit archer from 1,0 to 1,1\n" +
				"BLUE moved unit archer from 1,0 to 1,1\n"));
		
	}
	
	
	@Test
	public void testEndOfTurn() {
		CivTranscriptBuilder transcript = new CivTranscriptBuilder();
		transcript.buildEndOFTurn(Player.RED);
		String testEquals= transcript.transcriptOut();
		assertEquals (testEquals,"RED ended the turn\n");
	}
	
	//player + "moved unit" + unit.getTypeString() + "from " + from.getRow() + from.getColumn() + "to " + to.getRow() + to.getColumn();

}
